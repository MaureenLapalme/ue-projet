# UE projet

## Cahier de laboratoire 

### Contexte

Ce projet vise à étudier les réponses au niveau transcriptomique de l'espèce *Chondrus crispus* face à différents stress. 

### Matériel et méthode
Les données étaient fournies par Jonas Collen. Elles ont été récoltées dans le cadre d'une thèse. Ces données ont été échantillonnées à différents moments de la basse mer
avec trois réplicas par période. Ensuite, différents stress ont été induit, tout d'abord une réduction de l'intensité lumineuse, ensuite une augmentation de la salinité et
enfin une combination des deux conditions. 
Les données de transcriptomique récoltées à partir de ces expériences ont été normalisées et analysées sur le logiciel MeV.

### objectifs et questionnements
Peut-on déterminer des gènes surexprimés en stress lumineux (Le stress lumineux est considéré comme important lors des réplicas à 13h)? Y-t-il des gènes régulés selon une horloge interne?
Gènes liés au phycobilisome, heat shock protein

### 02.12.2019 et 03.12.2019 : Phase exploratoire

Ces deux premiers jours ont consisté à la prise en main du logiciel et du jeu de données. Nous avons tout d'abord réalisé des analyses sur l'ensemble du jeu de données,
notamment une ACP et un cluster hiérarchique.  
Ces analyses préliminaires nous ont permis d'éliminer le réplicas 15ha qui différait trop des deux autres et présentait des valeurs aberrantes.
Le jeu de données a donc été réduit pour étudier les variations des gènes au cours du temps.
On avait donc un jeu de donné qui contient les trois réplicas de 10h, les trois réplicas de 13h et les réplicats b et c de 15h.
Le but ici était de déterminer des groupes de gènes qui présentaient des variations entre les différentes conditions lumineuses. Pour cela, des clusters K-mean ont été réalisés sur le jeu
de données réduit. Parmi les différents clusters obtenus, un ou deux ont été sélectionné selon les différences d'expressions de gènes et des clusters K-mean étaient réalisés de nouveau
jusqu'à obtenir un nombre de gènes exploitable.
A partir de ce dernier cluster, une ANOVA à un facteur à été réalisée en utilisant trois groupes (chaque heures correspondant à un groupe), afin de savoir quels gènes étaient
significativement différents selon les heures de la journée.

### 04.12.2019

#### Première approche : Identification des protéines dont l'expression varie au cours du temps

Dans un premier temps, seules les données collectées en fontion des heures de la journée (10, 13 et 15 heures). Un K-Mean à 6 groupes a été effectué à partir des données totales (Figure X). 
Les gènes regroupés par le cluster 2 ont montré une tendance à la surexpression à 10 heures et à la sous expression à 13 et 15 heures. 

[Figure X]

Ce groupe a été isolé et un nouveau K-mean a été effectué avec cette fois 15 groupes pour séparer les gènes en petits groupes (Figure X).

[Figure X]

Le cluster 13 ayant montré une bonne cohérence entre ses différents gènes, les séquences de ces derniers ont été regroupées et blastées dans NCBI [https://blast.ncbi.nlm.nih.gov/Blast.cgi].
Les résultats sont présentés dans la rubrique ci-dessous.

#### Protéines sur exprimées à 10 heures et sous éxprimées à 13 heures

_CHC-T00000594001_ unnamed protein product

_CHC-T00001077001_ unnamed protein product

_CHC-T00001582001_ unnamed protein product

_CHC-T00001987001_ unnamed protein product

_CHC-T00002200001_ unnamed protein product

_CHC-T00002830001_ __phycobiliprotein lyase__

_CHC-T00002847001_ unnamed protein product

_CHC-T00003061001_ __putative phospholipid-transporting ATPase 7__

_CHC-T00004032001_ unnamed protein product

_CHC-T00004161001_ unnamed protein product

_CHC-T00004187001_ __Plant-specific TFIIB-related protein 1__

_CHC-T00004815001_ __protein phosphatase Slingshot homolog 2__

_CHC-T00004864001_ unnamed protein product

_CHC-T00004931001_ unnamed protein product

_CHC-T00005066001_ __Calcium-activated potassium channel slowpoke__

_CHC-T00005069001_ __Transmembrane 9 superfamily member 3__

_CHC-T00005127001_ __DNA repair helicase XPD__

_CHC-T00005557001_ __26S proteasome non-ATPase regulatory subunit 13__

_CHC-T00005658001_ unnamed protein product

_CHC-T00005696001_ __Store-operated calcium entry-associated regulatory factor__

_CHC-T00005931001_ unnamed protein product

_CHC-T00006572001_ unnamed protein product

_CHC-T00007919001_ __putative glucuronosyltransferase PGSIP8__

_CHC-T00008123001_ unnamed protein product

_CHC-T00008572001_ __ATP dependant DNA helicase__

_CHC-T00009082001_ __probable inositol 2-dehydrogenase__

_CHC-T00009477001_ __aspartate aminotransferase__

_CHC-T00010147001_ __40S ribosomal protein S4__

#### Protéines sur exprimées à 13 heures et sous éxprimées à 10 heures
_CHC-T00010056001_ __Glutamine-dependent carbamoyl-phosphate synthase. Aspartate carbamoyltransferase. Dihydroorotase__

_CHC-T00008256001_ unnamed protein

_CHC-T00008128001_ __phosphoribosyl-AMP cyclohydrolase__ 

_CHC-T00008118001_ unnamed protein

_CHC-T00008077001_ unnamed protein

_CHC-T00007965001_ unnamed protein

_CHC-T00007855001_ unnamed protein 

_CHC-T00007463001_ unnamed protein

_CHC-T00007160001_ __FAD containing oxidoreductase__ 

_CHC-T00006258001_

_CHC-T00006207001_ __endoplasmic reticulum metallopeptidase__ 

_CHC-T0005961001_ unnamed protein

_CHC-T00005803001_ __fidgetin-like protein__ 

_CHC-T00005752001_ unnamed protein

_CHC-T00005434001_ unnamed protein 

_CHC-T00005339001_ unnamed protein

_CHC-T00004852001_ __diaminopimelate epimerase__ 

_CHC-T00004512001_ __hydroxyisourate hydrolase__

_CHC-T00004413001_ __iron sulfur cluster__

_CHC-T00004198001_ unnamed protein

_CHC-T00003918001_ unnamed protein

_CHC-T00003855001_ unnamed protein

_CHC-T00003698001_ unnamed protein

_CHC-T00003395001_ unnamed protein

_CHC-T00002946001_ unnamed protein

_CHC-T00002861001_ __mitochondrial carrier__ 

_CHC-T00001794001_ unnamed protein

_CHC-T00001552001_ __protease__



#### Deuxième Approche : Recherche directe de gènes impliqués dans des voies métaboliques précises

__Les protéines en lien avec les acides gras__

On a recherché dans les protéines annotées celle qui étaient en lien avec les acides gras et on en a identifié 13.
Un K-mean à 4 clusters a été appliqué ce qui a permis d'isoler dans le cluster 3 cinq gènes ayant une cinétique similaire avec une transcription positive à 10
heures mais inhibée à 13 heures. Tous ces gènes significatifs selon l'ANOVA (95%). L'inhibition de ces gènes n'est pas retrouvée pour la condition expérimentale 13 heures avec 40% de lumière. On peut donc supposer que 
à 13 heures l'inhibition est bien due au stress lumineux et pas à l'heure de la journée.

__Les Heat shock proteins__

La même démarche a été mise en place que pour les protéines en lien avec les acides gras. Onze protéines ont été identifiées donc 8 se sont révélées être significatives
selon l'ANOVA (95%).

__Les protéines de la phase sombre de la photosynthèse__

Pour continuer sur l'hypothèse de la photo-inhibition, on a cherché des gènes impliqués dans la photosynthèse. On s'est donc intéressées aux gènes de la phase sombre de la photosynthèse.
Ainsi, 5 gènes ont été identifiés, tous significatifs par teste ANOVA. On a donc réalisé des histogrammes représentant les gènes dans les différentes conditions expérimentales. 

__Les protéines pigmentaires__

Le même processus a été suivi pour les protéines pigmentaires. En revanche, les proteines ont été séparées en deux groupes: les proteines lyases et les protéines "linker". 
Des histogrammes ont été réalisés pour les deux types de protéines. 


#### Troisième Approche : Etude ciblée sur les gènes liés aux stress

A partir d'une bibliographie fournie par notre tuteur, différentes catégories de gènes liés au stress ont été identifiées:
- les heat shock
- les gènes réactifs à l'oxygène
- les gènes liés au stress

Tous les gènes de ces catégories présents dans le jeu de données ont été extraits et observés sur MeV afin de voir d'éventuels patterns. 

### 05.12.2019

A partir des différentes approches ciblées la veille, différents graphiques de variation d'expression des gènes ont été réalisés sur Excel.
Cela nous a permis d'identifier des groupes de gènes qui ne montraient pas de variations particulières et d'autres qui à l'inverse en montraient.
Ainsi, les résultats obtenus à partir des graphiques sont les suivants:
- les gènes liés au stress ne montrent pas de variation particulière
- les heat shock proteins ne montrent pas non plus de variation
- les gènes liés au cycle de calvin diminuent à 13h 

Ces résultats relativement inattendus nous ont donc amenées à faire des recherches bibliographiques.
Tout cela nous a amené à la conclusion que _Chondrus crispus_ n'était pas en condition de stress pendant l'expérience mais qu'il y avait malgré tout un processus de 
photoinhibition à 13h. 

Nous avons donc réalisé notre diaporama pour la présentation orale et commencé la rédaction du rapport final.
